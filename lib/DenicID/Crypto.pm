package DenicID::Crypto;

use strict;
use warnings;

use Data::Dumper;

use Crypt::JWT qw/encode_jwt/;

use Crypt::OpenSSL::RSA;
use Crypt::PK::RSA;
use MIME::Base64::URLSafe;
use Digest::SHA 'sha256';

use JSON::XS;

sub new {
	my $class = shift;

	return bless {}, $class;
}

sub generate {
	my ($self, $type) = @_;

	my $rsa = Crypt::OpenSSL::RSA->generate_key(2048);

	return $rsa;
}

sub get_jwt {
	my ($self, $headers, $payload, $rsa) = @_;

	my $jwt_string = encode_jwt(payload => $payload, alg => 'RS256', key => $rsa, extra_headers => $headers);

	my ($header, undef, $signature) = split(/\./, $jwt_string);

	return ($header . ".." . $signature);
}

sub get_public_jwk {
	my ($self, $rsa) = @_;

	my $temp = $rsa->get_public_key_string();
	my $pk = Crypt::PK::RSA->new(\$temp);

	my $jwk_string = $pk->export_key_jwk('public');

	my $json;
	eval {
		$json = decode_json($jwk_string);
	}; if ($@) {
	}

	return $json;
}

sub get_acme_challenge {
	my ($self, $token, $rsa) = @_;

	my $temp = $rsa->get_public_key_string();
	my $pk = Crypt::PK::RSA->new(\$temp);

	return urlsafe_b64encode(sha256($token .'.' . $pk->export_key_jwk_thumbprint()));
}

1;
