package DenicID::Client;

use strict;
use warnings;

use Data::Dumper;

use DenicID::Agent;
use DenicID::Crypto;

use LWP::UserAgent;
use HTTP::Request;
use HTTP::Headers;
use HTTP::Response;

use JSON::XS qw/encode_json decode_json/;

sub new {
	my ($class) = @_;

	return bless {}, $class;
}

sub send {
	my ($self, $headers, $payload, $rsa) = @_;

	my $json_payload;
	eval {
		$json_payload = encode_json($payload);
	}; if ($@) {
	}

	my $req;
	if ($headers->{'method'} && $headers->{'method'} eq 'POST' &&
		$headers->{'url'}) {
		$req = HTTP::Request->new('POST', $headers->{'url'}, [], $json_payload);
	}
	
	my $key_class = new DenicID::Crypto();
	my $jwt = $key_class->get_jwt($headers, $payload, $rsa);

	$req->header('jws-detached-signature' => $jwt);
	$req->header('Content-Type' => 'application/json');

	$req->content($json_payload);

	my $ua = new LWP::UserAgent();
	my $response = $ua->request($req);

	print "REQUEST: " . $req->as_string() . "\n";
	
	if ($response->is_success) {
		my $json_response;
		eval {
			$json_response = decode_json($response->content());
		}; if ($@) {
			die Dumper $@;
		}

		return $json_response;
	}
	else {
		die Dumper $response;
	}
}

1;