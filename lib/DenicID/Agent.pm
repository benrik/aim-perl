package DenicID::Agent;

use strict;
use warnings;

use Data::Dumper;

use DenicID::Client;
use DenicID::Crypto;

sub new {
	my ($class, $rsa, $base_url) = @_;

	my $self = {
		'rsa' => $rsa,
		'base_url' => $base_url
	};
	return bless $self, $class;
}

sub create_agent_account {
	my ($self, $name, $full_name, $email) = @_;

	my $key_class = new DenicID::Crypto();
	my $jwk = $key_class->get_public_jwk($self->{'rsa'});
	$jwk->{'use'} = 'sig';

	my $headers = {
		'kid' => '',
		'method' => 'POST',
		'jwk' => $jwk,
		'url' => $self->{'base_url'} . '/agents',
		'use' => 'sig'
	};

	my $payload = {
		'name' => $name,
		'fullName' => $full_name,
		'email' => $email
	};

	my $denic_id_client = new DenicID::Client();
	my $response = $denic_id_client->send($headers, $payload, $self->{'rsa'});
	return $response;
}

1;